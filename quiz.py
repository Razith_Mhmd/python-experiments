questions = [
    {
        'question_id': 1,
        'question': 'What is capital of india?'
    },
    {
        'question_id': 2,
        'question': 'What is our national animal?'
    }
]
answers = [
    {
        'answer_id': 2,
        'question_id': 2,
        'choices': ['Cat', 'Tiger', 'Dog', 'Rat'],
        'answer_index': 1
    },
    {
        'answer_id': 1,
        'question_id': 1,
        'choices': ['Chennai', 'Mumbai', 'Kolkatta', 'Delhi'],
        'answer_index': 3
    }
]

question_length = len(questions) ## How many number of questions?
if question_length >= 1: ## if no questions skip the block
    
    ## Reading Questions one by one
    for question in questions: ## Loop the question till end\
        print(question['question']) ## print the question
        
        ## reading answers based on question id
        for answer in answers:
            if answer['question_id'] == question['question_id']:
                choice_index = 0
                while choice_index < len(answer['choices']):
                    print(answer['choices'][choice_index], end='\t')
                    choice_index += 1
                print()