# Title
print ("Grade Calculation")

# Get marks subject by subject
print('Enter the maths marks')
maths = input()
print('Enter the tamil marks')
tamil = input()
print('Enter the english marks')
english = input()
print('Enter the english marks')
science = input()

# Type Convert strings to Integer 
maths = int(maths)
tamil = int(tamil)
english = int(english)
science = int(science)

# Calculating Total of all subjects
total = maths + tamil + english + science
print('Total ' + str(total))

avg = total / 4
 
print('Average mark: ' + str(avg))

# Check pass or fail
if(maths < 35 or tamil < 35 or english < 35 or science < 35 ):
    print('Failed: Grade F')

# Check if number is inside the bound
elif (avg > 100 or avg < 0):
    print('Out of bound')

# If not fail and inside the bound
elif(avg > 90 and avg <= 100):
    print('Grade O')

elif(avg > 80 and avg <= 90):
    print('Grade A')

elif(avg > 60 and avg <= 80):
    print('Grade B')

elif(avg > 40 and avg <= 60):
    print('Grade C')

elif(avg > 30 and avg <= 40):
    print('Grade D')
               